from src.calculator import Calculator
from src.operations.sub import SubOperation
from src.operations.sum import SumOperation

calculator = Calculator(SumOperation(), SubOperation())

operation_1 = calculator.addition(1, 2, True)
operation_2 = calculator.subtraction(2, 1, True)

print(f'Operação 1: {operation_1}')
print(f'Operação 2: {operation_2}')
