class SubOperation(object):
    
    def sub(self, num1, num2):
        if num1 < num2:
            return num2 - num1
        return num1 - num2