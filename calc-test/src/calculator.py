class Calculator(object):
    
    def __init__(self, sum,sub):
        self.sum = sum
        self.sub = sub
        
    def addition(self, num1, num2, op):
        if op:
            return self.sum.sum(num1, num2)
        return None
    
    def subtraction(self, num1, num2, op):
        if op:
            return self.sub.sub(num1, num2)
        return None