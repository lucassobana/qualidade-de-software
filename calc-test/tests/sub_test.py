from src.operations.sub import SubOperation
from faker import Faker

faker = Faker()

def test_sub():
    subOperation = SubOperation()
    
    num_1 = faker.random_number()
    num_2 = faker.random_number()
    if num_1 < num_2:
        expected_sub = num_2 - num_1
    else:
        expected_sub = num_1 - num_2    
    
    
    result = subOperation.sub(num_1, num_2)
    
    assert result == expected_sub